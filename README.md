## SDPclient

```
composer require coredevina/sdpclient
```


##Lumen

add source below to  bootstrap/app.php
```
$app->register(Coredevina\Sdpclient\SdpServiceProvider::class);


if (!class_exists('Sdpclient')) {
    class_alias('\Coredevina\Sdpclient\SdpClient', 'Sdpclient');
}

....

$app->configure('sdp');

```

```
php artisan vendor:publish --provider="Coredevina\Sdpclient\SdpServiceProvider"
```


Change SDP baseUrl on config/sdp.php
```
return[
    'api' => 'http://sdp.local/index.php/api/'
];
```

SomeController.php
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sdpclient;

...
```

Use
```

$telco = new Sdpclient();
$telco->checkSubscribe($msisdn);

```
