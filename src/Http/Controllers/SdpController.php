<?php

namespace Coredevina\Sdpclient\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SdpController extends Controller
{
    /**
     * 'msisdn' => '6281210401298','content_id' => '1000010','type' => 'buy','lang' => 'ID','price' => '2000'
     */
    public function chargingMenu(Request $request){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => config('sdp.api').'newListMenu',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $request->all(),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function subscription(Request $request){

    }

    public function purchase(Request $request){

    }
}
