<?php
Route::group(['namespace'=>'Coredevina\Sdpclient\Http\Controllers','prefix'=>'api/v1'],function(){
    Route::post('chargingMenu','SdpController@chargingMenu')->name('chargingMenu');
    Route::post('subscription','SdpController@subscription')->name('subscription');
    Route::post('purchase','SdpController@purchase')->name('purchase');
});