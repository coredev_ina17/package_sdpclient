<?php
namespace Coredevina\Sdpclient;

use Illuminate\Support\ServiceProvider;

class SdpServiceProvider extends ServiceProvider
{
    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->mergeConfigFrom(
            __DIR__.'/config/Sdp.php','sdp'
        );
    }

    public function register(){

    }
}